<?php

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\ProductCollection;
use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\XMLReaderFeedClient;
use PHPUnit\Framework\TestCase;

class XMLReaderFeedClientTest extends TestCase
{
    /**
     * @param $url
     * @return void
     *
     * @dataProvider provideData
     */
    public function testReturnsProductsCollection($url): void
    {
        $client = new XMLReaderFeedClient($url);

        $this->assertInstanceOf(ProductCollection::class, $client->getProducts());
    }

    public function provideData()
    {
        return [
            [
                'https://feedink-public.s3.eu-central-1.amazonaws.com/static/dev_sample_feed.xml',
            ],
        ];
    }
}
