<?php

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\Product;
use Jakubrusinowicz\Feedink\ProductImageGenerator\GDProductImageGenerator;
use PHPUnit\Framework\TestCase;

class GDProductImageGeneratorTest extends TestCase
{
    public function testGeneratorReturnsImageContent()
    {
        $stub = $this->createStub(Product::class);

        $stub->method('getTitle')
            ->willReturn('test');

        $stub->method('getPrice')
            ->willReturn(1.0);

        $stub->method('getPhoto')
            ->willReturn('https://picsum.photos/seed/picsum/800/1200.jpg');

        $generator = new GDProductImageGenerator();

        $this->assertIsString($generator->generate($stub));
    }
}
