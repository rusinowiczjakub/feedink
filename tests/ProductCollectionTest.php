<?php

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\Product;
use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\ProductCollection;
use PHPUnit\Framework\TestCase;

class ProductCollectionTest extends TestCase
{
    public function testThrowsExceptionWhenAddOtherItemTypeThanProductInstance(): void
    {
        $collection = new ProductCollection();
        $this->expectException(\Webmozart\Assert\InvalidArgumentException::class);
        $collection->add('test');
    }

    public function testCanAddProductInstance(): void
    {
        $product = $this->createStub(Product::class);
        $collection = new ProductCollection();
        $collection->add($product);

        $this->assertInstanceOf(Product::class, $collection->get(0));
    }
}
