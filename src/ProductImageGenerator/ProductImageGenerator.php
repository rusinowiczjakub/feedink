<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator;

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\Product;

interface ProductImageGenerator
{
    public function generate(
        Product $product
    ): string;
}
