<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator;

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\Product;

final class GDProductImageGenerator implements ProductImageGenerator
{
    public function generate(
        Product $product
    ): string
    {
        $fileName = $this->temporarySaveBaseImage($product->getPhoto());
        $image = imagecreatefromjpeg($fileName);
        $resizedImage = $this->resizeImage($image, $fileName);
        unlink($fileName);

        $whiteColor = imagecolorallocate($image, 255, 255, 255);

        $this->addColorBoxToImage($resizedImage, 0, 150);
        $this->addColorBoxToImage($resizedImage, 900, 100);
        $this->addTitleTextToImage($resizedImage, $whiteColor, $product->getTitle());
        $this->addPriceTextToImage($resizedImage, $whiteColor, $product->getPrice());

        ob_start();
        imagejpeg($resizedImage);
        return ob_get_clean();
    }

    private function resizeImage(\GdImage $image, string $filename): \GdImage
    {
        $resizedImage = imagecreatetruecolor(1000, 1000);
        [$width, $height] = getimagesize($filename);
        imagecopyresampled(
            $resizedImage,
            $image,
            0,
            0,
            0,
            0,
            1000,
            1000,
            $width,
            $height
        );

        return $resizedImage;
    }

    private function addTitleTextToImage(\GdImage $image, int $color, string $title)
    {
        imagettftext(
            $image,
            25,
            0,
            20,
            90,
            $color,
            '/System/Library/Fonts/Supplemental/Arial.ttf',
            $title
        );
    }

    private function addPriceTextToImage(\GdImage $image, int $color, float $price)
    {
        imagettftext(
            $image,
            25,
            0,
            20,
            960,
            $color,
            '/System/Library/Fonts/Supplemental/Arial.ttf',
            number_format($price, 2, '.')
        );
    }

    private function addColorBoxToImage(\GdImage $baseImage, int $positionY, int $height)
    {
        $im = imagecreatetruecolor(1000, 150);

        $box = imagecolorallocate($im, 128, 128, 128);
        imagefill($im, 0, 0, $box);

        imagecopy($baseImage, $im, 0, $positionY, 0, 0, 1000, $height);
    }

    private function temporarySaveBaseImage(string $productImageUrl): string
    {
        $opts = [
            'http' => [
                "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
            ]
        ];
        $context = stream_context_create($opts);
        $imageContent = file_get_contents($productImageUrl, false, $context);

        $fileName = __DIR__ . '/../../image.' . last(explode('.', $productImageUrl));
        file_put_contents($fileName, $imageContent);

        return $fileName;
    }
}
