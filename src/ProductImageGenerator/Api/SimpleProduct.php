<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Api;

final class SimpleProduct implements Product
{
    public function __construct(
        private readonly string $id,
        private readonly string $title,
        private readonly float $price,
        private readonly string $photoUrl
    )
    {

    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getPhoto(): string
    {
        return $this->photoUrl;
    }

}
