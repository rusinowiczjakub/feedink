<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Api;

interface FeedClient
{
    public function getProducts(): ProductCollection;
}
