<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Api;

final class XMLReaderFeedClient implements FeedClient
{
    public function __construct(
        private string $feedUrl
    )
    {
    }

    public function getProducts(): ProductCollection
    {
        /** @var \XMLReader $xml */
        $xml = \XMLReader::open($this->feedUrl);

        $products = new ProductCollection();
        while ($xml->read()) {
            if ($xml->name !== 'record') {
                continue;
            }

            $record = new \SimpleXMLElement($xml->readOuterXml());

            if (!(string) $record->id) {
                continue;
            }

            $products->add(new SimpleProduct(
                id: (string) $record->id,
                title: (string) $record->title,
                price: (float) $record->price,
                photoUrl: (string) $record->image_link
            ));
        }

        return $products;
    }
}
