<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Api;

use Illuminate\Support\Collection;
use Webmozart\Assert\Assert;

final class ProductCollection extends Collection
{

    public function __construct(Product ...$products)
    {
        parent::__construct($products);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function add($product): self
    {
        Assert::isInstanceOf($product, Product::class);

        parent::add($product);
        return $this;
    }
}
