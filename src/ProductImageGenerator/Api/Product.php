<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Api;

interface Product
{
    public function getId(): string;

    public function getTitle(): string;

    public function getPrice(): float;

    public function getPhoto(): string;
}
