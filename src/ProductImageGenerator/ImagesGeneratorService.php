<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator;

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\FeedClient;
use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\Product;

class ImagesGeneratorService
{
    public function __construct(
        private readonly FeedClient $client,
        private readonly ProductImageGenerator $generator,
    )
    {

    }

    public function generateImages()
    {
        $products = $this->client->getProducts();

        /** @var Product $product */
        foreach ($products as $product) {
            $image = $this->generator->generate(
                $product
            );


            file_put_contents(__DIR__."/../../storage/{$product->getId()}.jpg", $image);
        }
    }
}
