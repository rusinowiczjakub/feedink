<?php

namespace Jakubrusinowicz\Feedink\ProductImageGenerator\Ui\Console\ProductImageGenerator;

use Jakubrusinowicz\Feedink\ProductImageGenerator\Api\XMLReaderFeedClient;
use Jakubrusinowicz\Feedink\ProductImageGenerator\GDProductImageGenerator;
use Jakubrusinowicz\Feedink\ProductImageGenerator\ImagesGeneratorService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'product-image-generator:generate')]
final class GenerateProductsImages extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = new XMLReaderFeedClient(
            'https://feedink-public.s3.eu-central-1.amazonaws.com/static/dev_sample_feed.xml'
        );

        $generator = new GDProductImageGenerator();

        (new ImagesGeneratorService($client, $generator))->generateImages();

        return Command::SUCCESS;
    }
}
