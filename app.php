<?php

require __DIR__ . '/vendor/autoload.php';

use Jakubrusinowicz\Feedink\ProductImageGenerator\Ui\Console\ProductImageGenerator\GenerateProductsImages;
use Symfony\Component\Console\Application;


$application = new Application();

$application->add(new GenerateProductsImages());

$application->run();
