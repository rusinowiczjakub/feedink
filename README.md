# Product Images Generator

Project requires PHP Version: 8.1

## Running with locally installed PHP

1. Installing dependencies

```
$ composer install
```

2. Runnning generator

```
$ php app.php product-image-generator:generate
```

2. Running tests

```
$ php vendor/bin/phpunit tests
```
